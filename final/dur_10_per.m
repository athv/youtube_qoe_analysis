clear;
load 'second_dataset_clear'
sessions = new_sessions;


%% ta sessions pou anikoun sto 10 percentile
tmp1 = cell2mat(sessions(:, 9));
% % short videos
 Y = prctile(tmp1,10)
 videos_in_range=(tmp1 <= Y) ;
 
k=1;
for i=1:1:size(videos_in_range)
    if(videos_in_range(i,1) == 1 )
        lala(k,:) = sessions(i,:);
        k = k+1;
    end
end

clearvars sessions;
sessions = lala;

clearvars lala tmp1 watc_ratio_sessions watc_perc_sessions twentyperc_watching_sessions tmp_watching_perc buff_events_new tmp_buff_new tm num_buffs rebuf_duration rebuf_ratio has_buff has_not_buff_ratio k i;
clearvars Y videos_in_range tmp_startup_delay startup_delay has_buff_not_rebuff targe_susp buf_num_susp session_duration video_duration fin_watc_time tmp_watching_time twomin_watching_sessions;


%% linoume to provlime me ta NaN values sta adverts
tmp = sessions(:,14);
size_tmp = size(tmp)
for i=1:1:size_tmp
    if( isempty(sessions{i,14}) == 0 )
        if(isempty(sessions{i,14}{1,2}) == 1)
            sessions{i,14}{1,2} = '0';
        end    
    end
end
clearvars  size_tmp i

%% code for new sessions
% Estimate the number and the duration of the rebuffering events for each
% session

buff_events_new =sessions(:,4); 
tmp_buff_new = cellfun(@str2double, buff_events_new, 'uni', false);
tm = sessions(:,6);
[num_buffs rebuf_duration rebuf_ratio] = cellfun(@buff_duration, tmp_buff_new, tm);

% Check for the sessions with rebuffering event at the end of the video
has_buff = find(num_buffs > 0);
has_not_buff_ratio = find(rebuf_ratio == 0); 
has_buff_not_rebuff = intersect(has_buff, has_not_buff_ratio);

targe_susp = cell2mat(sessions(has_buff_not_rebuff, 8));
buf_num_susp = num_buffs(has_buff_not_rebuff)

%%%%%%%%%%%%%kathisteriseis apo diafimiseis%%%%%%%%%%%%%%%%%%%%%
adv_events_new =sessions(:,14); 
tmp_adv_new = cellfun(@str2double, adv_events_new, 'uni', false);
adv_tm = sessions(:,6);
[num_advs adv_duration adv_ratio] = cellfun(@buff_duration, tmp_adv_new, adv_tm);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Estimate the video watching percentage
session_duration = cell2mat(sessions(:, 6));
video_duration = cell2mat(sessions(:, 9));

tmp_startup_delay = cell2mat(sessions(:,7));
startup_delay = tmp_startup_delay/1000;

fin_watc_time = session_duration - rebuf_duration - startup_delay - adv_duration;

startup_delay = cell2mat(sessions(:,7));
% Estimate the Video Watching percentage
watc_ratio_sessions = fin_watc_time ./ video_duration;
watc_perc_sessions = watc_ratio_sessions * 100;


% Bitrate changes of the sessions: positive negative and the max level of
% the BR changes
bc_chan = sessions(:,5);
[pos_br neg_br max_level] = cellfun(@bitrate_changes, bc_chan);

% Mean weightes resolution of each session
[mean_weighte_res] = cellfun(@avgResolution, bc_chan, tm);

% target scenarios is the type that a session ended
target_scenarios = cell2mat(sessions(:,8));

% Find ids for sessions without RB
ids_no_rb = find(num_buffs == 0);

% Find ids for sessions with RB
ids_rb = find(num_buffs > 0);

% Find ids for sessions with no BR, positive or negative changes  
ids_no_pos_br = find(pos_br == 0);
ids_pos_br = find(pos_br > 0);

ids_no_neg_br = find(neg_br == 0);
ids_neg_br = find(neg_br > 0);
ids_no_br = intersect(ids_no_pos_br, ids_no_neg_br);

% Scenario No RB/No BR
ids_scen0 = intersect(ids_no_br, ids_no_rb);
scen0_watch_perc = mean(watc_perc_sessions(ids_scen0))
scen0_watch_perc_med = median(watc_perc_sessions(ids_scen0))
std_0 = std(watc_perc_sessions(ids_scen0))
res_scen_0 = mean(mean_weighte_res(ids_scen0))
res_std_scen_0 = std(mean_weighte_res(ids_scen0))
targ_0 = target_scenarios(ids_scen0)
abd_ratio_0 = length(find(targ_0 ~=0)) / length(targ_0)

% Find the ids for sessions with RBs and no BRs without considering the ratio
ids_scen1 = intersect(ids_no_br, ids_rb);
scen1_watch_perc = mean(watc_perc_sessions(ids_scen1))
scen1_watch_perc_med = median(watc_perc_sessions(ids_scen1))
res_std_scen_1 = std(mean_weighte_res(ids_scen1))
std_1 = std(watc_perc_sessions(ids_scen1))
res_scen_1 = mean(mean_weighte_res(ids_scen1))
res_std_scen_0 = std(mean_weighte_res(ids_scen1))


% CDF & histogram of rebuffering ratio
tmp_rebuf_r = rebuf_ratio(ids_scen1);
tmp_num_rebufs = num_buffs(ids_scen1);
tmp_rebuf_duration = rebuf_duration(ids_scen1);
tmp_has_buff = find(tmp_num_rebufs > 0);
tmp_has_not_buff_ratio = find(tmp_rebuf_r == 0);
tmp_has_buff_not_rebuff = intersect(tmp_has_buff, tmp_has_not_buff_ratio);

[f_rbr x_rbr] = ecdf(tmp_rebuf_r);
% figure;
% plot(x_rbr, f_rbr, 'Linewidth', 1.5)
% xlabel('Rebuffering ratio')
% ylabel('CDF')
% 
% figure
% histogram(tmp_rebuf_r, 'Normalization', 'Probability')
% xlabel('Rebuffering ratio')
% ylabel('Probability')

% Scenario Last sec RB/No BR
scen1_0 =find(tmp_rebuf_r ==0);
ids_scen1_0 = ids_scen1(scen1_0);
scen1_0_watch_perc = mean(watc_perc_sessions(ids_scen1_0))
scen1_0_watch_perc_med = median(watc_perc_sessions(ids_scen1_0))
std_1_0 = std(watc_perc_sessions(ids_scen1_0))
res_scen_1_0 = mean(mean_weighte_res(ids_scen1_0))
res_std_scen_1_0 = std(mean_weighte_res(ids_scen1_0))
targ_1_0 = target_scenarios(ids_scen1_0)
abd_ratio_1_0 = length(find(targ_1_0 ~=0)) / length(targ_1_0)


% Scenario Low RB Ratio/No BR
scen1_05 =find(tmp_rebuf_r >0 & tmp_rebuf_r <=0.05);
%tmp_scen1_05_susp = setdiff(scen1_05, );
ids_scen1_05 = ids_scen1(scen1_05);
scen1_05_watch_perc = mean(watc_perc_sessions(ids_scen1_05))
scen1_05_watch_perc_med = median(watc_perc_sessions(ids_scen1_05))
std_1_05 = std(watc_perc_sessions(ids_scen1_05))
res_scen_1_05 = mean(mean_weighte_res(ids_scen1_05))
res_std_scen_1_05 = std(mean_weighte_res(ids_scen1_05))
targ_1_05 = target_scenarios(ids_scen1_05)
abd_ratio_1_05 = length(find(targ_1_05 ~=0)) / length(targ_1_05)

% Scenario Medium RB Ratio/No BR
tmp_scen1_03 =find(tmp_rebuf_r >0.05 & tmp_rebuf_r <=0.2);

tmpscen1_03 = setdiff(tmp_scen1_03, scen1_05);
ids_scen1_03 = ids_scen1(tmpscen1_03);
scen1_03_watch_perc = mean(watc_perc_sessions(ids_scen1_03))
scen1_03_watch_perc_med = median(watc_perc_sessions(ids_scen1_03))
res_scen_1_03 = mean(mean_weighte_res(ids_scen1_03))
res_std_scen_1_03 = std(mean_weighte_res(ids_scen1_03))

std_1_03 = std(watc_perc_sessions(ids_scen1_03))
targ_1_03 = target_scenarios(ids_scen1_03)
abd_ratio_1_03 = length(find(targ_1_03 ~=0)) / length(targ_1_03)

% Scenario High RB Ratio/No BR
ids_scen1_1 = find(tmp_rebuf_r > 0.2);
scen1_1_watch_perc = mean(watc_perc_sessions(ids_scen1(ids_scen1_1)))
scen1_1_watch_perc_med = median(watc_perc_sessions(ids_scen1(ids_scen1_1)))
res_scen_1_1 = mean(mean_weighte_res(ids_scen1(ids_scen1_1)))
res_std_scen_1_1 = std(mean_weighte_res(ids_scen1_1))
std_1_1 = std(watc_perc_sessions(ids_scen1(ids_scen1_1)))
targ_1_1 = target_scenarios(ids_scen1(ids_scen1_1))
abd_ratio_1_1 = length(find(targ_1_1 ~=0)) / length(targ_1_1)

% Scenario No RB/BR+
ids_only_pos = intersect(ids_pos_br, ids_no_neg_br); 
ids_scen2 = intersect(ids_no_rb, ids_only_pos);
scen2_watch_perc = mean(watc_perc_sessions(ids_scen2))
scen2_watch_perc_med = median(watc_perc_sessions(ids_scen2))
std_2 = std(watc_perc_sessions(ids_scen2))
res_scen_2 = mean(mean_weighte_res(ids_scen2))
res_std_scen_2 = std(mean_weighte_res(ids_scen2))
targ_2 = target_scenarios(ids_scen2)
abd_ratio_2 = length(find(targ_2 ~=0)) / length(targ_2)

% Scenario No RB/BR-
ids_only_neg = intersect(ids_neg_br, ids_no_pos_br); 
ids_scen3 = intersect(ids_no_rb, ids_only_neg); 
scen3_watch_perc = mean(watc_perc_sessions(ids_scen3))
scen3_watch_perc_med = median(watc_perc_sessions(ids_scen3))
std_3 = std(watc_perc_sessions(ids_scen3))
res_scen_3 = mean(mean_weighte_res(ids_scen3))
res_std_scen_3 = std(mean_weighte_res(ids_scen3))
targ_3 = target_scenarios(ids_scen3)
abd_ratio_3 = length(find(targ_3 ~=0)) / length(targ_3)

% Scenario RB/BR+
ids_only_pos = intersect(ids_pos_br, ids_no_neg_br); 
ids_scen4 = intersect(ids_rb, ids_only_pos);
scen4_watch_perc = mean(watc_perc_sessions(ids_scen4))
scen4_watch_perc_med = median(watc_perc_sessions(ids_scen4))
std_4 = std(watc_perc_sessions(ids_scen4))
res_scen_4 = mean(mean_weighte_res(ids_scen4))
res_std_scen_4 = std(mean_weighte_res(ids_scen4))
rb_dur_4 = median(rebuf_duration(ids_scen4))
rbr_4 = median(rebuf_ratio(ids_scen4))
targ_4 = target_scenarios(ids_scen4)
abd_ratio_4 = length(find(targ_4 ~=0)) / length(targ_4)
pos_br_has_last_sec_reb = cellfun(@last_sec_rebuf, tmp_buff_new(ids_scen4), tm(ids_scen4))

% ids for sessions with RB and BR-
ids_only_neg = intersect(ids_neg_br, ids_no_pos_br); 
ids_scen5 = intersect(ids_rb, ids_only_neg);
scen5_watch_perc = mean(watc_perc_sessions(ids_scen5))
scen5_watch_perc_med = median(watc_perc_sessions(ids_scen5))
std_5 = std(watc_perc_sessions(ids_scen5))
res_scen_5 = mean(mean_weighte_res(ids_scen5))
res_std_scen_5 = std(mean_weighte_res(ids_scen5))
rb_dur_5 = median(rebuf_duration(ids_scen5))
rbr_5 = median(rebuf_ratio(ids_scen5))
targ_5 = target_scenarios(ids_scen5)
abd_ratio_5 = length(find(targ_5 ~=0)) / length(targ_5)


num_bufs_5 =  num_buffs(ids_scen5);
ids_only_one_scen_5 = ids_scen5(find(num_bufs_5==1));
% Scenario RB/Early BR-
is_early_5 = cellfun(@find_interval_rebuf_resol, bc_chan(ids_only_one_scen_5), tmp_buff_new(ids_only_one_scen_5));
early_ids = (ids_only_one_scen_5(find(is_early_5==1)))
scen5early_watch_perc = mean(watc_perc_sessions(early_ids))
scen5early_watch_perc_med = median(watc_perc_sessions(early_ids))
std_5_early = std(watc_perc_sessions(early_ids))
res_scen_5early = mean(mean_weighte_res(early_ids))
res_std_scen_5early = std(mean_weighte_res(early_ids))
rb_dur_5_early = median(rebuf_duration(early_ids))
rbr_5_early = mean(rebuf_ratio(early_ids))
targ_5_early = target_scenarios(early_ids)
abd_ratio_5_early = length(find(targ_5_early ~=0)) / length(targ_5_early)
early_has_last_sec_reb = cellfun(@last_sec_rebuf, tmp_buff_new(early_ids), tm(early_ids))

% Scenario RB/Other BR- 
other_ids = setdiff(ids_only_one_scen_5, early_ids)
scen5other_watch_perc = mean(watc_perc_sessions(other_ids))
scen5other_watch_perc_med = median(watc_perc_sessions(other_ids))
std_5_other = std(watc_perc_sessions(other_ids))
res_scen_5other = mean(mean_weighte_res(other_ids))
res_std_scen_5other = std(mean_weighte_res(other_ids))
rb_dur_5_other = median(rebuf_duration(other_ids))
rbr_5_other = mean(rebuf_ratio(other_ids))
targ_5_other = target_scenarios(other_ids)
abd_ratio_5_other = length(find(targ_5_other ~=0)) / length(targ_5_other)
has_last_sec_reb = cellfun(@last_sec_rebuf, tmp_buff_new(other_ids), tm(other_ids))


% Create cell array with sessions watching percentage per scenario
all_ses_reb_ratio = {};

all_ses_reb_ratio{1} = watc_perc_sessions(ids_scen0);
all_ses_reb_ratio{2} = watc_perc_sessions(ids_scen1_0);
all_ses_reb_ratio{3} = watc_perc_sessions(ids_scen1_05);
all_ses_reb_ratio{4} = watc_perc_sessions(ids_scen1_03);
all_ses_reb_ratio{5} = watc_perc_sessions(ids_scen1(ids_scen1_1));
all_ses_reb_ratio{6} = watc_perc_sessions(ids_scen2);
all_ses_reb_ratio{7} = watc_perc_sessions(ids_scen3);
all_ses_reb_ratio{8} = watc_perc_sessions(ids_scen4);
all_ses_reb_ratio{9} = watc_perc_sessions(early_ids);
all_ses_reb_ratio{10} = watc_perc_sessions(other_ids);

% less scenarios - these are only the scenarios that appear in  table 2

less_scen_reb_ratio = {};
less_scen_reb_ratio{1} = watc_perc_sessions(ids_scen0);
less_scen_reb_ratio{2} = watc_perc_sessions(ids_scen1);
less_scen_reb_ratio{3} = watc_perc_sessions(ids_scen2);
less_scen_reb_ratio{4} = watc_perc_sessions(ids_scen3);
less_scen_reb_ratio{5} = watc_perc_sessions(ids_scen4);
less_scen_reb_ratio{6} = watc_perc_sessions(ids_scen5);




% Create cell array with sessions mean weighted resolution
all_ses_resol_rbr= {};
all_ses_resol_rbr{1} = mean_weighte_res(ids_scen0);
all_ses_resol_rbr{2} = mean_weighte_res(ids_scen1_0);
all_ses_resol_rbr{3} = mean_weighte_res(ids_scen1_05);
all_ses_resol_rbr{4} = mean_weighte_res(ids_scen1_03);
all_ses_resol_rbr{5} = mean_weighte_res(ids_scen1(ids_scen1_1));
all_ses_resol_rbr{6} = mean_weighte_res(ids_scen2);
all_ses_resol_rbr{7} = mean_weighte_res(ids_scen3);
all_ses_resol_rbr{8} = mean_weighte_res(ids_scen4);

all_ses_resol_rbr{9} = mean_weighte_res(early_ids);
all_ses_resol_rbr{10} = mean_weighte_res(other_ids);
 


% Create cell array with sessions median weighted resolution
resol_rbr_median= [];
resol_rbr_median(1) = median(mean_weighte_res(ids_scen0));
resol_rbr_median(2) = median(mean_weighte_res(ids_scen1_0));
resol_rbr_median(3) = median(mean_weighte_res(ids_scen1_05));
resol_rbr_median(4) = median(mean_weighte_res(ids_scen1_03));
resol_rbr_median(5) = median(mean_weighte_res(ids_scen1(ids_scen1_1)));
resol_rbr_median(6) = median(mean_weighte_res(ids_scen2));
resol_rbr_median(7) = median(mean_weighte_res(ids_scen3));
resol_rbr_median(8) = median(mean_weighte_res(ids_scen4));

resol_rbr_median(9) = median(mean_weighte_res(early_ids));
resol_rbr_median(10) = median(mean_weighte_res(other_ids));

 
% The following arrays contains the mean and median values of the video watching percentage 
% of each scenario

% watching_perc: The mean video watching percentage of each scenario 
watching_perc = [scen0_watch_perc scen1_0_watch_perc scen1_05_watch_perc scen1_03_watch_perc scen1_1_watch_perc...
    scen2_watch_perc scen3_watch_perc scen4_watch_perc scen5early_watch_perc scen5other_watch_perc]

% abandonment_ratio: The abandonment ratio of each scenario
abandonment_ratio = [abd_ratio_0 abd_ratio_1_0 abd_ratio_1_05 abd_ratio_1_03 abd_ratio_1_1 abd_ratio_2 abd_ratio_3 abd_ratio_4 abd_ratio_5_early abd_ratio_5_other]

% watching_perc_std: The standard deviation video watching percentage of each scenario
watching_perc_std = [std_0 std_1_0 std_1_05 std_1_03 std_1_1 std_2 std_3 std_4 std_5_early std_5_other]

% scenarios_size: The number of sessions for each scenario
scenarios_size = [length(ids_scen0) length(ids_scen1_0) length(ids_scen1_05) length(ids_scen1_03) length(ids_scen1_1) ...
     length(ids_scen2) length(ids_scen3) length(ids_scen4) length(early_ids) length(other_ids)]

% The median video watching percentage of each scenario
watching_perc_med = [scen0_watch_perc_med scen1_0_watch_perc_med scen1_05_watch_perc_med scen1_03_watch_perc_med scen1_1_watch_perc_med...
    scen2_watch_perc_med scen3_watch_perc_med scen4_watch_perc_med scen5early_watch_perc_med scen5other_watch_perc_med]

% Mean weighted resolution and the standard deviation for each scenario
resolu_tot = [res_scen_0 res_scen_1_0 res_scen_1_05 res_scen_1_03 res_scen_1_1 res_scen_2 res_scen_3 res_scen_4 res_scen_5early res_scen_5other]
resolu_std = [res_std_scen_0 res_std_scen_1_0 res_std_scen_1_05 res_std_scen_1_03 res_std_scen_1_1 res_std_scen_2 res_std_scen_3 res_std_scen_4 res_std_scen_5early res_std_scen_5other]

% For the less scenarios thenumber of sessions
less_scenarios_size = [length(ids_scen0) length(ids_scen1) length(ids_scen2) length(ids_scen3) length(ids_scen4) length(ids_scen5) ]
less_scenarios_perc = [length(ids_scen0)/length(watc_perc_sessions) length(ids_scen1)/length(watc_perc_sessions) length(ids_scen2)/length(watc_perc_sessions) length(ids_scen3)/length(watc_perc_sessions)...
    length(ids_scen4)/length(watc_perc_sessions) length(ids_scen5)/length(watc_perc_sessions) ] * 100
% Statistics 
stats_1_05 = median(rebuf_duration(ids_scen1_05))
stats_1_03 = median(rebuf_duration(ids_scen1_03))
stats_1_1 = median(rebuf_duration(ids_scen1(ids_scen1_1)))
stats_4 = median(rebuf_duration(ids_scen4))
stats_5_early = median(rebuf_duration(early_ids))
stats_5_other = median(rebuf_duration(other_ids))

stats_1_05 = mean(rebuf_duration(ids_scen1_05))
stats_1_03 = mean(rebuf_duration(ids_scen1_03))
stats_1_1 = mean(rebuf_duration(ids_scen1(ids_scen1_1)))
stats_4 = mean(rebuf_duration(ids_scen4))
stats_5_early = mean(rebuf_duration(early_ids))
stats_5_other = mean(rebuf_duration(other_ids))

