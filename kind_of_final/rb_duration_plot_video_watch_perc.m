%% Plot  Video Watching Percentage - RB duration
% Considering the watching_perc array plot the video watching percentage
% for the different RB duration
new_X = [0 watching_perc];

% name = {'0: Rebuf=0, BR^{+}=0, BR^{-}=0, RES>=large';...
name = {'';...
'No RB/No BR';...
'Low RB Dur/No BR';...
'High RB Dur/No BR';...
'No RB/ BR^{+}';...
'No RB/BR^{-}';...
'RB/BR^{+}';...
'RB/Early BR^{-}';...
'RB/Other BR^{-}';...
};

figure()
xlabel('Scenarios')
ylabel('Video watching percentage')
new_X = new_X./100;
abandonment_array = new_X;

XStd = [0 watching_perc_std]./100;

b=bar(diag(new_X),'stacked');
set(b(1),'facecolor','w')
set(b(2),'facecolor','w')
set(b(3),'facecolor',[255 215 0]./255)      % yellow 
set(b(4),'facecolor','r')
set(b(5),'facecolor','g')
set(b(6),'facecolor','g')
set(b(7),'facecolor','r')
set(b(8),'facecolor','r')
set(b(9),'facecolor','r')

12345
x = 1:9;
str_1 = num2str(scenarios_size(2));
str_2 = num2str(scenarios_size(3));
str_3 = num2str(scenarios_size(4));
str_4 = num2str(scenarios_size(5));

           
text(x(5),abandonment_array(5),'BR^{+}',...
               'HorizontalAlignment','center',...
               'VerticalAlignment','bottom','FontSize',20)
text(x(6),abandonment_array(6),'BR^{-}',...
               'HorizontalAlignment','center',...
               'VerticalAlignment','bottom','FontSize',20)
text(x(7),abandonment_array(7),'BR^{+}',...
               'HorizontalAlignment','center',...
               'VerticalAlignment','bottom','FontSize',20)
text(x(8),abandonment_array(8),{'Early';'BR^{-}'},...
               'HorizontalAlignment','center',...
               'VerticalAlignment','bottom','FontSize',20)
text(x(9),abandonment_array(9),{'Other';'BR^{-}'},...
               'HorizontalAlignment','center',...
               'VerticalAlignment','bottom','FontSize',20)

                
xlabel('Scenarios')
ylabel('Video watching percentage')

set(gca,'xticklabel',name)
set(gca,'FontSize',25, 'FontWeight','Bold');
set(gca,'XTickLabelRotation',30)

set(gca,'YTick',0:0.2:1, 'YTickLabel',0:20:100)

%set(gca,'XTickLabelRotation',20)
hold on;
errorbar([0  new_X(2) new_X(3) new_X(4) new_X(5) new_X(6) new_X(7) new_X(8) new_X(9) ],...
[0 XStd(2) XStd(3) XStd(4) XStd(5) XStd(6) XStd(7) XStd(8) XStd(9) ],'.');

xlim([1.2 9.8])
