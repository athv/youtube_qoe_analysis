%% about cond 1 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

clear;
load 'second_dataset_clear'
sessions = new_sessions;

%
tmp = sessions(:,14);
size_tmp = size(tmp)
for i=1:1:size_tmp
    if( isempty(sessions{i,14}) == 0 )
        if(isempty(sessions{i,14}{1,2}) == 1)
            sessions{i,14}{1,2} = '0';
        end    
    end
end
clearvars  size_tmp i tmp
%
% Estimate the number and the duration of the rebuffering events for each
% session

buff_events_new =sessions(:,4); 
tmp_buff_new = cellfun(@str2double, buff_events_new, 'uni', false);
tm = sessions(:,6);
[num_buffs rebuf_duration rebuf_ratio] = cellfun(@buff_duration, tmp_buff_new, tm);

% Check for the sessions with rebuffering event at the end of the video
has_buff = find(num_buffs > 0);
has_not_buff_ratio = find(rebuf_ratio == 0); 
has_buff_not_rebuff = intersect(has_buff, has_not_buff_ratio);

targe_susp = cell2mat(sessions(has_buff_not_rebuff, 8));
buf_num_susp = num_buffs(has_buff_not_rebuff)

%%%%%%%%%%%%%kathisteriseis apo diafimiseis%%%%%%%%%%%%%%%%%%%%%
adv_events_new =sessions(:,14); 
tmp_adv_new = cellfun(@str2double, adv_events_new, 'uni', false);
adv_tm = sessions(:,6);
[num_advs adv_duration adv_ratio] = cellfun(@buff_duration, tmp_adv_new, adv_tm);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Estimate the video watching percentage
session_duration = cell2mat(sessions(:, 6));
video_duration = cell2mat(sessions(:, 9));

tmp_startup_delay = cell2mat(sessions(:,7));
startup_delay = tmp_startup_delay/1000;

fin_watc_time = session_duration - rebuf_duration - startup_delay - adv_duration;

%%%watching time over 2 mins
tmp_watching_time=(fin_watc_time >= 120) ;
k=1;
for i=1:1:size(tmp_watching_time)
    if(tmp_watching_time(i,1) == 1 )
        twomin_watching_sessions(k,:) = sessions(i,:);
        k = k+1;
    end
end

clearvars sessions;
sessions = twomin_watching_sessions;

clearvars adv_duration adv_ratio adv_tm watc_ratio_sessions watc_perc_sessions twentyperc_watching_sessions tmp_watching_perc buff_events_new tmp_buff_new tm num_buffs rebuf_duration rebuf_ratio has_buff has_not_buff_ratio k i;
clearvars adv_events_new num_advs tmp tmp_adv_new tmp_startup_delay startup_delay has_buff_not_rebuff targe_susp buf_num_susp session_duration video_duration fin_watc_time tmp_watching_time twomin_watching_sessions;

% code for new sessions
% Estimate the number and the duration of the rebuffering events for each
% session

buff_events_new =sessions(:,4); 
tmp_buff_new = cellfun(@str2double, buff_events_new, 'uni', false);
tm = sessions(:,6);
[num_buffs rebuf_duration rebuf_ratio] = cellfun(@buff_duration, tmp_buff_new, tm);

% Check for the sessions with rebuffering event at the end of the video
has_buff = find(num_buffs > 0);
has_not_buff_ratio = find(rebuf_ratio == 0); 
has_buff_not_rebuff = intersect(has_buff, has_not_buff_ratio);

targe_susp = cell2mat(sessions(has_buff_not_rebuff, 8));
buf_num_susp = num_buffs(has_buff_not_rebuff)

%%%%%%%%%%%%%kathisteriseis apo diafimiseis%%%%%%%%%%%%%%%%%%%%%
adv_events_new =sessions(:,14); 
tmp_adv_new = cellfun(@str2double, adv_events_new, 'uni', false);
adv_tm = sessions(:,6);
[num_advs adv_duration adv_ratio] = cellfun(@buff_duration, tmp_adv_new, adv_tm);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Estimate the video watching percentage
session_duration = cell2mat(sessions(:, 6));
video_duration = cell2mat(sessions(:, 9));

tmp_startup_delay = cell2mat(sessions(:,7));
startup_delay = tmp_startup_delay/1000;

fin_watc_time = session_duration - rebuf_duration - startup_delay - adv_duration;

startup_delay = cell2mat(sessions(:,7));
% Estimate the Video Watching percentage
watc_ratio_sessions = fin_watc_time ./ video_duration;
C1_watc_perc_sessions = watc_ratio_sessions * 100;

clearvars adv_duration adv_events_new adv_ratio adv_tm buf_num_susp buff_events_new fin_watc_time has_buff has_buff_not_rebuff has_not_buff_ratio num_advs;
clearvars sessions num_buffs rebuf_duration rebuf_ratio session_duration startup_delay targe_susp tm tmp_adv_new tmp_buff_new tmp_startup_delay video_duration watc_ratio_sessions; 


%% about cond 2 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

sessions = new_sessions;

%
tmp = sessions(:,14);
size_tmp = size(tmp)
for i=1:1:size_tmp
    if( isempty(sessions{i,14}) == 0 )
        if(isempty(sessions{i,14}{1,2}) == 1)
            sessions{i,14}{1,2} = '0';
        end    
    end
end
clearvars  size_tmp i tmp
%
% Estimate the number and the duration of the rebuffering events for each
% session

buff_events_new =sessions(:,4); 
tmp_buff_new = cellfun(@str2double, buff_events_new, 'uni', false);
tm = sessions(:,6);
[num_buffs rebuf_duration rebuf_ratio] = cellfun(@buff_duration, tmp_buff_new, tm);

% Check for the sessions with rebuffering event at the end of the video
has_buff = find(num_buffs > 0);
has_not_buff_ratio = find(rebuf_ratio == 0); 
has_buff_not_rebuff = intersect(has_buff, has_not_buff_ratio);

targe_susp = cell2mat(sessions(has_buff_not_rebuff, 8));
buf_num_susp = num_buffs(has_buff_not_rebuff)

%%%%%%%%%%%%%kathisteriseis apo diafimiseis%%%%%%%%%%%%%%%%%%%%%
adv_events_new =sessions(:,14); 
tmp_adv_new = cellfun(@str2double, adv_events_new, 'uni', false);
adv_tm = sessions(:,6);
[num_advs adv_duration adv_ratio] = cellfun(@buff_duration, tmp_adv_new, adv_tm);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
session_duration = cell2mat(sessions(:, 6));
video_duration = cell2mat(sessions(:, 9));

tmp_startup_delay = cell2mat(sessions(:,7));
startup_delay = tmp_startup_delay/1000;

fin_watc_time = session_duration - rebuf_duration - startup_delay - adv_duration;


% Estimate the video watching percentage

watc_ratio_sessions = fin_watc_time ./ video_duration;
watc_perc_sessions = watc_ratio_sessions * 100;

%%%watching time over 2 mins
tmp_watching_perc=(watc_perc_sessions >= 20 ) ;
k=1;
for i=1:1:size(tmp_watching_perc)
    if(tmp_watching_perc(i,1) == 1 )
        twentyperc_watching_sessions(k,:) = sessions(i,:);
        k = k+1;
    end
end

clearvars sessions;
sessions = twentyperc_watching_sessions;

clearvars adv_duration adv_ratio adv_tm watc_ratio_sessions watc_perc_sessions twentyperc_watching_sessions tmp_watching_perc buff_events_new tmp_buff_new tm num_buffs rebuf_duration rebuf_ratio has_buff has_not_buff_ratio k i;
clearvars adv_events_new num_advs tmp tmp_adv_new tmp_startup_delay startup_delay has_buff_not_rebuff targe_susp buf_num_susp session_duration video_duration fin_watc_time tmp_watching_time twomin_watching_sessions;


% code for new sessions
% Estimate the number and the duration of the rebuffering events for each
% session

buff_events_new =sessions(:,4); 
tmp_buff_new = cellfun(@str2double, buff_events_new, 'uni', false);
tm = sessions(:,6);
[num_buffs rebuf_duration rebuf_ratio] = cellfun(@buff_duration, tmp_buff_new, tm);

% Check for the sessions with rebuffering event at the end of the video
has_buff = find(num_buffs > 0);
has_not_buff_ratio = find(rebuf_ratio == 0); 
has_buff_not_rebuff = intersect(has_buff, has_not_buff_ratio);

targe_susp = cell2mat(sessions(has_buff_not_rebuff, 8));
buf_num_susp = num_buffs(has_buff_not_rebuff)

%%%%%%%%%%%%%kathisteriseis apo diafimiseis%%%%%%%%%%%%%%%%%%%%%
adv_events_new =sessions(:,14); 
tmp_adv_new = cellfun(@str2double, adv_events_new, 'uni', false);
adv_tm = sessions(:,6);
[num_advs adv_duration adv_ratio] = cellfun(@buff_duration, tmp_adv_new, adv_tm);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Estimate the video watching percentage
session_duration = cell2mat(sessions(:, 6));
video_duration = cell2mat(sessions(:, 9));

tmp_startup_delay = cell2mat(sessions(:,7));
startup_delay = tmp_startup_delay/1000;

fin_watc_time = session_duration - rebuf_duration - startup_delay - adv_duration;

startup_delay = cell2mat(sessions(:,7));
% Estimate the Video Watching percentage
watc_ratio_sessions = fin_watc_time ./ video_duration;
C2_watc_perc_sessions = watc_ratio_sessions * 100;
 
clearvars adv_duration adv_events_new adv_ratio adv_tm buf_num_susp buff_events_new fin_watc_time has_buff has_buff_not_rebuff has_not_buff_ratio num_advs;
clearvars num_buffs rebuf_duration rebuf_ratio session_duration startup_delay targe_susp tm tmp_adv_new tmp_buff_new tmp_startup_delay video_duration watc_ratio_sessions; 

%%
clear;
load 'second_dataset_clear'
sessions = new_sessions;

%%
tmp = sessions(:,14);
size_tmp = size(tmp)
for i=1:1:size_tmp
    if( isempty(sessions{i,14}) == 0 )
        if(isempty(sessions{i,14}{1,2}) == 1)
            sessions{i,14}{1,2} = '0';
        end    
    end
end
clearvars  size_tmp i tmp
%%
% Estimate the number and the duration of the rebuffering events for each
% session

buff_events_new =sessions(:,4); 
tmp_buff_new = cellfun(@str2double, buff_events_new, 'uni', false);
tm = sessions(:,6);
[num_buffs rebuf_duration rebuf_ratio] = cellfun(@buff_duration, tmp_buff_new, tm);

% Check for the sessions with rebuffering event at the end of the video
has_buff = find(num_buffs > 0);
has_not_buff_ratio = find(rebuf_ratio == 0); 
has_buff_not_rebuff = intersect(has_buff, has_not_buff_ratio);

targe_susp = cell2mat(sessions(has_buff_not_rebuff, 8));
buf_num_susp = num_buffs(has_buff_not_rebuff)

%%%%%%%%%%%%%kathisteriseis apo diafimiseis%%%%%%%%%%%%%%%%%%%%%
adv_events_new =sessions(:,14); 
tmp_adv_new = cellfun(@str2double, adv_events_new, 'uni', false);
adv_tm = sessions(:,6);
[num_advs adv_duration adv_ratio] = cellfun(@buff_duration, tmp_adv_new, adv_tm);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
session_duration = cell2mat(sessions(:, 6));
video_duration = cell2mat(sessions(:, 9));

tmp_startup_delay = cell2mat(sessions(:,7));
startup_delay = tmp_startup_delay/1000;

fin_watc_time = session_duration - rebuf_duration - startup_delay - adv_duration;


% Estimate the video watching percentage

watc_ratio_sessions = fin_watc_time ./ video_duration;
watc_perc_sessions = watc_ratio_sessions * 100;

%%%watching time over 2 mins
tmp_watching_perc=(watc_perc_sessions >= 20 ) ;
k=1;
for i=1:1:size(tmp_watching_perc)
    if(tmp_watching_perc(i,1) == 1 )
        twentyperc_watching_sessions(k,:) = sessions(i,:);
        k = k+1;
    end
end

clearvars sessions;
sessions = twentyperc_watching_sessions;

clearvars adv_duration adv_ratio adv_tm watc_ratio_sessions watc_perc_sessions twentyperc_watching_sessions tmp_watching_perc buff_events_new tmp_buff_new tm num_buffs rebuf_duration rebuf_ratio has_buff has_not_buff_ratio k i;
clearvars adv_events_new num_advs tmp tmp_adv_new tmp_startup_delay startup_delay has_buff_not_rebuff targe_susp buf_num_susp session_duration video_duration fin_watc_time tmp_watching_time twomin_watching_sessions;


%% code for new sessions
clearvars sessions;
sessions = new_sessions;

% Estimate the number and the duration of the rebuffering events for each
% session

buff_events_new =sessions(:,4); 
tmp_buff_new = cellfun(@str2double, buff_events_new, 'uni', false);
tm = sessions(:,6);
[num_buffs rebuf_duration rebuf_ratio] = cellfun(@buff_duration, tmp_buff_new, tm);

% Check for the sessions with rebuffering event at the end of the video
has_buff = find(num_buffs > 0);
has_not_buff_ratio = find(rebuf_ratio == 0); 
has_buff_not_rebuff = intersect(has_buff, has_not_buff_ratio);

targe_susp = cell2mat(sessions(has_buff_not_rebuff, 8));
buf_num_susp = num_buffs(has_buff_not_rebuff)

%%%%%%%%%%%%%kathisteriseis apo diafimiseis%%%%%%%%%%%%%%%%%%%%%
adv_events_new =sessions(:,14); 
tmp_adv_new = cellfun(@str2double, adv_events_new, 'uni', false);
adv_tm = sessions(:,6);
[num_advs adv_duration adv_ratio] = cellfun(@buff_duration, tmp_adv_new, adv_tm);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Estimate the video watching percentage
session_duration = cell2mat(sessions(:, 6));
video_duration = cell2mat(sessions(:, 9));

tmp_startup_delay = cell2mat(sessions(:,7));
startup_delay = tmp_startup_delay/1000;

fin_watc_time = session_duration - rebuf_duration - startup_delay - adv_duration;

startup_delay = cell2mat(sessions(:,7));
% Estimate the Video Watching percentage
watc_ratio_sessions = fin_watc_time ./ video_duration;
watc_perc_sessions = watc_ratio_sessions * 100;


% Bitrate changes of the sessions: positive negative and the max level of
% the BR changes
bc_chan = sessions(:,5);
[pos_br neg_br max_level] = cellfun(@bitrate_changes, bc_chan);

% Mean weightes resolution of each session
[mean_weighte_res] = cellfun(@avgResolution, bc_chan, tm);

% target scenarios is the type that a session ended
target_scenarios = cell2mat(sessions(:,8));

% Find ids for sessions without RB
ids_no_rb = find(num_buffs == 0);

% Find ids for sessions with RB
ids_rb = find(num_buffs > 0);

% Find ids for sessions with no BR, positive or negative changes  
ids_no_pos_br = find(pos_br == 0);
ids_pos_br = find(pos_br > 0);

ids_no_neg_br = find(neg_br == 0);
ids_neg_br = find(neg_br > 0);
ids_no_br = intersect(ids_no_pos_br, ids_no_neg_br);


% Find the ids for sessions with RBs and no BRs without considering the ratio
ids_scen1 = intersect(ids_no_br, ids_rb);
scen1_watch_perc = mean(watc_perc_sessions(ids_scen1))
scen1_watch_perc_med = median(watc_perc_sessions(ids_scen1))
res_std_scen_1 = std(mean_weighte_res(ids_scen1))
std_1 = std(watc_perc_sessions(ids_scen1))
res_scen_1 = mean(mean_weighte_res(ids_scen1))
res_std_scen_0 = std(mean_weighte_res(ids_scen1))


% CDF & histogram of rebuffering ratio
tmp_rebuf_r = rebuf_ratio(ids_scen1);
tmp_num_rebufs = num_buffs(ids_scen1);
tmp_rebuf_duration = rebuf_duration(ids_scen1);
tmp_has_buff = find(tmp_num_rebufs > 0);
tmp_has_not_buff_ratio = find(tmp_rebuf_r == 0);
tmp_has_buff_not_rebuff = intersect(tmp_has_buff, tmp_has_not_buff_ratio);

% Scenario Low RB Ratio/No BR
scen1_05 =find(tmp_rebuf_r >0 & tmp_rebuf_r <=0.05);
ids_scen1_05 = ids_scen1(scen1_05);
scen1_05_watch_perc = watc_perc_sessions(ids_scen1_05);


% Scenario High RB Ratio/No BR
ids_scen1_1 = find(tmp_rebuf_r > 0.2);
watc_perc_sessions(ids_scen1(ids_scen1_1));
scen1_1_watch_perc_med = watc_perc_sessions(ids_scen1(ids_scen1_1));



%%
c1 = ecdf(C1_watc_perc_sessions);
c2 = ecdf(C2_watc_perc_sessions);
[h, p] = kstest2(scen1_1_watch_perc_med,scen1_05_watch_perc);



















