function [ avg_res ] = avgResolution( bitrates , timelength )

    brs = [str2double(bitrates(:,1)) cellfun(@bitrate2rate, bitrates(:,2))];
    
    num_changes = size(brs,1);
    
    total_nom = 0;
    total_den = 0;
    
    if ( num_changes>1 )
        for i = 1:num_changes
           
            if( i==num_changes )
                time_diff = timelength-brs(i,1);
                total_nom = total_nom + time_diff*brs(i,2);
                total_den = total_den + time_diff;
            end
            
            if ( i>1 )
                time_diff = brs(i,1)-brs(i-1,1);
                total_nom = total_nom + time_diff*brs(i-1,2);
                total_den = total_den + time_diff;
            end

        end
        avg_res = total_nom/total_den;
    else
        avg_res = brs(1,2);
    end

    
         
    
end

