function [ flag ] = last_sec_rebuf( buffering_events, timelength )
%LAST_SEC_REBUF 
%Inputs 
% buffering_events: buffering events of each session
% timelength: total duration of the session
%Outputs
%flag = 0 if the last rebuffering of the session did not happened during the last
%second of the session
%flag = 1 if the last rebuffering of the session happened during the last
%second of the session

[num_of_buffs col] = size(buffering_events);
 flag =0;   
% Case no-rebuffering sessions 
if(num_of_buffs == col & col == 1 & isnan(buffering_events(:,1)))
    flag = 0;
else
    tmp1 = buffering_events(:,1);
    if(tmp1(end) == timelength)
        flag = 1;
    end
end
    
end

