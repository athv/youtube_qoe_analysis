function [ rate ] = bitrate2rate( x ) 

    if strcmp(x,'tiny')
        y=80;
    elseif strcmp(x,'small')
        y=400;
    elseif strcmp(x,'medium')
        y=1000;
    elseif strcmp(x,'large')
        y=2500;
    elseif strcmp(x,'hd720')
        y=5000;
    elseif strcmp(x,'hd1080')
        y=8000;
    elseif strcmp(x,'hd1440')
        y=10000;
    elseif strcmp(x,'hd2160')
        y=20000;
    else
        y=0;
    end

    rate = y;
end

