function [ count_pos, count_neg, max_step ] = bitrate_changes( br_changes )
%BITRATE_CHANGES Summary of this function goes here
%   Detailed explanation goes here
[num_of_brs col] = size(br_changes);
    
% 
if(num_of_brs == col & col == 1 & isnan(br_changes(:,1)))
    num_of_brs = -1;
    1111111
        
elseif(num_of_brs == col & col == 1 )
    num_of_brs = -2;
    22222222
% Case multiple BR
else
    tmp1 = br_changes(:,1);
    tmp2 = br_changes(:,2);
    
    
    if(strcmp(tmp2(1),'unknown'))
        tmp2(1)
        length(tmp1)
        
        tmp1 = tmp1(2:end);
        tmp2 = tmp2(2:end);
        
    end
    kbps = zeros(1,length(tmp2));
    level = zeros(1, length(tmp2));
    for i = 1:length(tmp2)
        if(strcmp(tmp2(i),'tiny'))
          kbps(i)=80;
          level(i) = 1;
        elseif(strcmp(tmp2(i),'small'))
          kbps(i)=400;
          level(i) = 2;
        elseif(strcmp(tmp2(i),'medium'))
          kbps(i)=1000;
          level(i) = 3;
        elseif(strcmp(tmp2(i),'large'))
          kbps(i)=2500;
          level(i) = 4;
        elseif(strcmp(tmp2(i),'hd720'))
          kbps(i)=5000;
          level(i) = 5;
        elseif(strcmp(tmp2(i),'hd1080'))
          kbps(i)=8000;
          level(i) = 6;
        elseif(strcmp(tmp2(i),'hd1440'))
          kbps(i)=10000;
          level(i) = 7;
        elseif(strcmp(tmp2(i),'hd2160'))
          kbps(i)=35000;
          level(i) = 8;
        else
%             123456789
%             tmp2(i)
            kbps(i)=-1;
            level(i) = NaN;
        end
    end
    level
    count_pos= 0;
    count_neg= 0; 
    count_same= 0;
    level_changes= [];
    if(length(level)>1)
    for i = 1:(length(kbps)-1)
        lev_diff = level(i+1) - level(i);
        level_changes = [level_changes lev_diff];
        
        if(kbps(i+1) > kbps(i))
            count_pos = count_pos +1; 
        elseif(kbps(i+1) < kbps(i))
            count_neg = count_neg +1;
        else
            count_same = count_same +1;
        end
    end
      
    [step, id] = max(abs(level_changes));
    level_changes
    max_step = level_changes(id(1));
    else
    
    max_step = 0;
    end
    end

end

