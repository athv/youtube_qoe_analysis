function [ num_of_buffs duration ratio] = buff_duration( buff_cell, timelength )
%BUFF_DUR Summary of this function goes here
%   Detailed explanation goes here

[num_of_buffs col] = size(buff_cell);
    
% Case no-rebuffering sessions 
if(num_of_buffs == col & col == 1 & isnan(buff_cell(:,1)))
    num_of_buffs = 0;
    duration = 0;
    ratio = 0;
else
%     
% %   
% elseif(num_of_buffs == col & col == 1 )
%     num_of_buffs = 1;
%     duration = -1;
%     
% % Case multiple rebufferings 
% else
    tmp1 = buff_cell(:,1);
    tmp2 = buff_cell(:,2);
    if(length(tmp2) ~= num_of_buffs || isnan(tmp2(num_of_buffs)))
        123456
        tmp2(num_of_buffs) = (timelength) - (tmp1(num_of_buffs))
    end
    
%     % Check/shorten rebufferings with high/wrong duration
%     fin_times_rebuf = tmp1+tmp2;
%     
%     for i = 1:length(fin_times_rebuf)
%         % Last rebufering 
%         if(i == length(fin_times_rebuf))
%             timelength;
%             if((fin_times_rebuf(i)) > (timelength))
%                 tmp2(i) = timelength - tmp1(i);
%             end
%         % Previous rebufferings 
%         elseif((fin_times_rebuf(i)) > (tmp1(i+1)))
%             tmp2(i) = tmp1(i+1) - tmp1(i);
%         end
%     end 
    
    duration = sum(tmp2);
    ratio = duration / timelength;
end
end

